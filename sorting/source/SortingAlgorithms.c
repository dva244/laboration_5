#define _CRT_SECURE_NO_WARNINGS
#include "SortingAlgorithms.h"
#include "Statistics.h"
#include "Array.h"
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>


int isImplemented(SortingAlgorithm algorithm)
{
    switch (algorithm)
    {
        case BUBBLE_SORT:
        case INSERTION_SORT:
        case SELECTION_SORT:
        case QUICK_SORT:
        case MERGE_SORT:
            return 1;
        default:
            return 0;
    }
}

/*Antalet byten i en och samma algoritm kan vara olika beroende p� implementationen. Ibland ligger datat redan p� r�tt plats och d� kan man v�lja att testa det och inte g�ra ett byte (vilket ger extra j�mf�relse) eller s� kan man �nd� g�ra ett byte (med sig sj�lv). F�ljer man de algoritmer som vi g�tt igenom p� f�rel�sningarna exakt s� g�r man en swap �ven p� ett element som ligger p� r�tt plats

  N�r du analyserar det data som genereras (result.txt) s� beh�ver du ha detta i �tanke */

/******************************************************************************************/
/* Era algoritmer har: */
//=============================================================================
/* bubble sort that pushes small values forward */
static void bubbleSort(ElementType* arrayToSort, unsigned int size, Statistics* statistics)
{
    int i;
    int runs = 0;
    int swaps;

    while(lessThan(runs, size - 1, statistics))
    {
        swaps = 0;
        
        //after each run the first element doesn't have to be checked as we know it is the smallest that's been pushed forward and is in it's right postion
        for(i = size - 1; greaterThan(i, runs, statistics); i--)
        {
            if(lessThan(arrayToSort[i], arrayToSort[i-1], statistics))
            {
                swapElements(&arrayToSort[i-1], &arrayToSort[i], statistics);
                swaps++;
            }
        }

        if(equalTo(swaps, 0, statistics)) // no swaps happend in an entire run, it is sorted
        {
            return;
        }

        runs++;
    }
}
//=============================================================================
/* left sorted array (consists of the first element at the begining), picks first from right array, finds it's right
 * position in the left, shifts elements infront of that spot forward and places it in it's right spot, and
 * steps up the split*/
static void insertionSort(ElementType* arrayToSort, unsigned int size, Statistics* statistics)
{
    int i;
    int smaller;
    int right;
    int temp;

    for(i = 1; lessThan(i, size, statistics); i++)
    {
        smaller = i - 1;

        //finds the element in the "left array" that is smaller than element "i", so we know where to place i
        while((lessThan(arrayToSort[i], arrayToSort[smaller], statistics)) && (greaterThan(smaller, 0, statistics)))
        {
            smaller--;
        }

        //as to not shift 1 step to far
        if(greaterThan(arrayToSort[i], arrayToSort[smaller], statistics))
        {
            smaller++;
        }

        temp = arrayToSort[i];
        right = i;  //so we dont step up i which is used for the greater for loop

        //shuffles elements forward to make place for i
        while(greaterThan(right, smaller, statistics))
        {
            arrayToSort[right] = arrayToSort[right-1];
            right--;
        }
        arrayToSort[smaller] = temp;
    }
}
//=============================================================================
/* finds smallest element in right, swaps it with the first element in right and steps up split */
static void selectionSort(ElementType* arrayToSort, unsigned int size, Statistics* statistics)
{
    int i;
    int split = 0;
    int *min;

    while(lessThan(split, size - 1, statistics))
    {
        min = &arrayToSort[split];
        
        for(i = split; lessThan(i, size, statistics); i++)  //looping through the right array to find the smallest element
        {
            if(lessThan(arrayToSort[i], *min, statistics))
            {
                min = &arrayToSort[i];
            }
        }
        swapElements(min, &arrayToSort[split], statistics); //swaps the smallest element in the right array with the first element in the right array and steps up the split
        split++;
    }
}
//=============================================================================
static void merge(ElementType* arrayToSort, ElementType* firstarray, ElementType* secondarray, int start, int mid, int end, Statistics* statistics)
{
    int i = 0;
    int f = 0;
    int s = 0;

    // compares if left or right is smaller and takes the smallest and places first in the array that is being sorted
    while((lessThan(i, end, statistics)) && (lessThan(f, mid, statistics)) && (lessThan(s, end-mid, statistics)))
    {
        if(lessThan(firstarray[f], secondarray[s], statistics))
        {
            arrayToSort[i] = firstarray[f];
            f++;
        }
        else
        {
            arrayToSort[i] = secondarray[s];
            s++;
        }
        i++;
    }

    /*when all elements in left or right has been put in the array that is being sorted, the rest of the elements in the
    array that hasn't been fully emptied yet is put at the end*/

    while((lessThan(f, mid, statistics)) && (lessThan(i, end, statistics)))
    {
        arrayToSort[i] = firstarray[f];
        f++;
        i++;
    }

    while((lessThan(s, end-mid, statistics)) && (lessThan(i, end, statistics)))
    {
        arrayToSort[i] = secondarray[s];
        s++;
        i++;
    }
}
//=============================================================================
/* divides the entire array and then builds it up again by placing everything sorted */
static void mergeSort(ElementType* arrayToSort, unsigned int size, Statistics* statistics)
{
    int mid;
    int i = 0;
    int k = 0;
    int rightsize;
    int *left;
    int *right;

    //divides depending on if it's even or uneven
    if(equalTo(size % 2, 0, statistics))
    {
        mid = size/2;
        rightsize = size-mid;
    }
    else
    {
        mid = (size - 1)/2;
        rightsize = size - mid;
    }

    left = (int*)malloc(sizeof(int)*mid);
    right = (int*)malloc(sizeof(int)*rightsize);
    //should check if allocation was succesful before proceeding (left != NULL, right != NULL)

    // moves elements in the left half to the left array
    while(lessThan(i, mid, statistics))
    {
        left[i] = arrayToSort[i];
        i++;
    }

    // moves elements in the right half to the right array
    while(lessThan(i, size, statistics))
    {
        right[k] = arrayToSort[i];
        i++;
        k++;
    }

    //continues to divide recursivly until they are indivdual elements which are by definiton sorted
    if(greaterThan(size, 2, statistics))
    {
        mergeSort(left, mid, statistics);
        mergeSort(right, rightsize, statistics);
    }

    //merges all the divided sorted into bigger sorted until the entire thing is sorted(recursivly)
    merge(arrayToSort, left, right, 0, mid, size, statistics);
    free(left);
    free(right);
}
//=============================================================================
/* first element is taken as pivot */
static int help(ElementType* arrayToSort, int size, Statistics* statistics)
{
    int *pivot = &arrayToSort[0];
    int split = 1;
    int i = 1;

    while(lessThan(i, size, statistics))
    {
        if(lessThan(arrayToSort[i], *pivot, statistics))    // if it's smaller than pivot swap it with the first element in the right and step up the split
        {
            swapElements(&arrayToSort[i], &arrayToSort[split], statistics);
            split++;
        }
        i++;
    }    
    //moving pivot to its right position
    swapElements(pivot, &arrayToSort[split-1], statistics);
    pivot = &arrayToSort[split-1];

    return split-1;
}
//=============================================================================
/* takes an element(in this implementation the first) and makes sure smaller elements are to the left and bigger elements are to the right of it */
static void quickSort(ElementType* arrayToSort, unsigned int size, Statistics* statistics)
{
    int pivotindex = help(arrayToSort, size, statistics);

    // recusivly does pivot spliting
    if(greaterThan(size, 1, statistics))
    {
        quickSort(arrayToSort, pivotindex, statistics);
        quickSort(&arrayToSort[pivotindex+1], size-pivotindex-1, statistics);
    }
}
//=============================================================================
/******************************************************************************************/


char* getAlgorithmName(SortingAlgorithm algorithm)
{
    /* Ar inte strangen vi allokerar lokal for funktionen?
       Nej, inte i detta fall. Strangkonstanter ar ett speciallfall i C */
    switch (algorithm)
    {
        case BUBBLE_SORT:	 return "  Bubble sort ";
        case SELECTION_SORT: return "Selection sort";
        case INSERTION_SORT: return "Insertion sort";
        case MERGE_SORT:	 return "  Merge sort  ";
        case QUICK_SORT:	 return "  Quick sort  ";
        default: assert(0 && "Ogiltig algoritm!"); return "";
    }
}

// Sorterar 'arrayToSort' med 'algorithmToUse'. Statistik for antal byten och jamforelser hamnar i *statistics
static void sortArray(ElementType* arrayToSort, unsigned int size, SortingAlgorithm algorithmToUse, Statistics* statistics)
{
    if(statistics != NULL)
        resetStatistics(statistics);

    switch (algorithmToUse)
    {
        case BUBBLE_SORT:	 bubbleSort(arrayToSort, size, statistics); break;
        case SELECTION_SORT: selectionSort(arrayToSort, size, statistics); break;
        case INSERTION_SORT: insertionSort(arrayToSort, size, statistics); break;
        case MERGE_SORT:	 mergeSort(arrayToSort, size, statistics); break;
        case QUICK_SORT:	 quickSort(arrayToSort, size, statistics); break;
        default:
                             assert(0 && "Ogiltig algoritm!");
    }
}

// Forbereder arrayer for sortering genom att allokera minne for dem, samt intialisera dem
static void prepareArrays(SortingArray sortingArray[], SortingAlgorithm algorithm, const ElementType* arrays[], const unsigned int sizes[])
{
    assert(isImplemented(algorithm));

    int i;
    int totalArraySize;

    for (i = 0; i < NUMBER_OF_SIZES; i++)
    {
        totalArraySize = sizeof(ElementType)*sizes[i];
        sortingArray[i].arrayToSort = malloc(totalArraySize);
        memcpy(sortingArray[i].arrayToSort, arrays[i], totalArraySize);

        sortingArray[i].algorithm = algorithm;
        sortingArray[i].arraySize = sizes[i];
        resetStatistics(&sortingArray[i].statistics);
    }
}

// Sorterar arrayerna
static void sortArrays(SortingArray toBeSorted[])
{
    int i;
    for (i = 0; i < NUMBER_OF_SIZES; i++)
    {
        SortingArray* current = &toBeSorted[i];
        sortArray(current->arrayToSort, current->arraySize, current->algorithm, &current->statistics);

        if (!isSorted(current->arrayToSort, current->arraySize))
        {
            printf("Fel! Algoritmen %s har inte sorterat korrekt!\n", getAlgorithmName(current->algorithm));
            printf("Resultatet �r: \n");
            printArray(current->arrayToSort, current->arraySize, stdout);
            assert(0); // Aktiveras alltid
        }
    }
}

void printResult(SortingArray sortedArrays[], FILE* file)
{
    assert(file != NULL);

    int i;
    for (i = 0; i < NUMBER_OF_SIZES; i++)
    {
        fprintf(file, "%4d element: ", sortedArrays[i].arraySize);
        printStatistics(&sortedArrays[i].statistics, file);
        fprintf(file, "\n");
    }
    fprintf(file, "\n");
}

void sortAndPrint(SortingArray sortingArray[], SortingAlgorithm algorithm, const ElementType* arrays[], unsigned int sizes[], FILE* file)
{
    assert(file != NULL);

    prepareArrays(sortingArray, algorithm, arrays, sizes);
    sortArrays(sortingArray);
    printResult(sortingArray, file);
}

void freeArray(SortingArray sortingArray[])
{
    int i;
    for (i = 0; i < NUMBER_OF_SIZES; i++)
    {
        if (sortingArray[i].arrayToSort != NULL)
            free(sortingArray[i].arrayToSort);
        sortingArray[i].arrayToSort = NULL;
        sortingArray[i].arraySize = 0;
        resetStatistics(&sortingArray[i].statistics);
    }
}
